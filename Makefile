.phony: cleanup

all: \
	build/Agents\ of\ Fortune.pdf \
	build/Agents\ of\ Fortune-print.pdf \
	build/AOF01-Ghostlight.pdf \
	build/AOF01-Ghostlight-print.pdf \
	cleanup

cleanup:
	rm -f build/*.{log,aux,toc}

build/Agents\ of\ Fortune.pdf: books/core/Agents\ of\ Fortune.tex
	pdflatex --halt-on-error --output-directory build/ "$?"
	pdflatex --halt-on-error --output-directory build/ "$?"

build/Agents\ of\ Fortune-print.pdf: build/Agents\ of\ Fortune.pdf cover/blank.pdf books/core/cover-front.pdf books/core/cover-back.pdf
	pdftk books/core/cover-front.pdf cover/blank.pdf "build/Agents of Fortune.pdf" cover/blank.pdf books/core/cover-back.pdf cat output "$@"

build/AOF01-Ghostlight.pdf: books/aof01/AOF01-Ghostlight.tex
	pdflatex --halt-on-error --output-directory build/ "$?"
	pdflatex --halt-on-error --output-directory build/ "$?"

build/AOF01-Ghostlight-print.pdf: build/AOF01-Ghostlight.pdf cover/blank.pdf books/aof01/cover-front.pdf books/aof01/cover-back.pdf
	pdftk books/aof01/cover-front.pdf cover/blank.pdf build/AOF01-Ghostlight.pdf cover/blank.pdf books/aof01/cover-back.pdf cat output "$@"
