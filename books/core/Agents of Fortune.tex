% Copyright 2021-2022 John Anthony
% Released under a CC-BY-NC 3.0 license
% https://creativecommons.org/licenses/by-nc/3.0/

\documentclass[a5paper]{article}
\usepackage[margin=0.5in]{geometry}
\usepackage[table]{xcolor}
\usepackage{sectsty}
\usepackage{incgraph,tikz}
\usepackage{helvet}
\usepackage[graphicx]{realboxes}
\usepackage{enumitem}
\usepackage{eso-pic}
\usepackage{multicol}
\usepackage[T1]{fontenc}
\usepackage{titletoc}
\usepackage[some]{background}
\usepackage{hyperref}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}

\sectionfont{\centering}
\renewcommand{\familydefault}{\sfdefault}
\setlength{\parindent}{0em}
\setlength\columnsep{2em}
\pagenumbering{gobble}

\newcommand{\sectionx}[1]{
	\clearpage
	\addcontentsline{toc}{section}{\uppercase{#1}}
	\section*{\uppercase{#1}}
	\hrule
	\vspace{1.5em}
}

\newcommand{\subsectionx}[1]{
	\subsection*{\uppercase{#1}}
	\addcontentsline{toc}{subsection}{#1}
}

\newcommand{\e}[1]{\textbf{#1}}
\newcommand{\banshee}[1]{\textsc{banshee}}
\newcommand{\imgplaceholder}{
	\vfill
	\begin{center}
		\ \\
	\end{center}
	\vfill
}

\begin{document}

\begin{titlepage}
	\newgeometry{margin=0.25in,top=2cm}
	\begin{center}
		\huge
		\textbf{Agents of Fortune}
		\vspace{5pt}
		\small

		\vspace{1em}
		Rules-light roleplaying \\
		in the golden age of espionage

		\vspace{1em}
		\textbf{Version:} 1.2 \\
		\today

		\vfill
		\includegraphics[width=20em]{img/martini-glass.png}
		\vfill

		\vspace{2em}
		\textbf{Authors:} John Anthony \& Ben Fleetham \\
		\textbf{Editor:} John Anthony \\
		\textbf{Art:} Stacey Sanderson \\
		\vspace{1em}
		All text (including \LaTeX\  source code) and art \\
		is licensed under a CC BY-NC 3.0 license.
	\end{center}
	\restoregeometry
\end{titlepage}

\newpage
\newgeometry{margin=2cm,left=4cm,right=4cm}
\backgroundsetup{scale=1, opacity=1, angle=0, contents={
	\includegraphics[width=\paperwidth]{img/border}
}}
\BgThispage
\imgplaceholder
\begin{small}
	\tableofcontents
\end{small}
\imgplaceholder
\restoregeometry

%%%%%
\sectionx{About}
\backgroundsetup{contents={}}
\pagenumbering{arabic}
\subsectionx{What is Agents of Fortune?}
Agents of fortune is a roleplaying game of espionage and intrigue in the vein of those famous spy films and books, where players take on the role of MI6 agents who must foil the plans of notorious international criminals!

\begin{itemize}[noitemsep]
\item Designed to have mechanics which stay out of the way and let the players get on with being spies.
\item Interactionist in style, rewarding players for clever thinking and describing what they’re doing.
\item Built for immediate play, good for bringing your casual friends in for a game, playing at your local games store, or taking to the convention table.
\end{itemize}

If you've never heard of a roleplaying game, a quick web search can help you out.

\subsectionx{Player roles}
The player who runs the scenario is called the \e{Spymaster}, while everyone else takes on the role of an \e{agent}. The \e{Spymaster} is responsible for bringing the setting and scenario to life, and providing the \e{agents} with fair and impartial arbitration to the outcome of their actions.

\subsectionx{What stuff do I need to play?}
Players need a pencil and paper to record their \e{agents}, and it's recommended that everyone at the table intending to play an \e{agent} have three six-sided dice and two twenty-sided dice. The \e{Spymaster} needs a prepared scenario, pencil, and plenty of scratch paper for notes.

\subsectionx{Dice conventions}
Dice are referred to by size following the letter ``d''. For example, a twenty-sided dice is called a ``d20''. If you are asked to roll multiple dice, that number is given before the ``d''. For example: ``3d6'' means roll three six-sided dice. You will often be told to take the ``best'' or ``worst'' dice from multiple dice rolled -- best or worst in this case is from the perspective of the \e{agent}.

%%%%%
\sectionx{Making an agent}

\subsectionx{Attributes}
\e{Agents} are summarised by three \e{attributes}:

\begin{itemize}[noitemsep]
	\item \e{Body}, representing physical conditioning
	\item \e{Wits}, representing quick thinking
	\item \e{Will}, representing strength of determination
\end{itemize}

To determine each \e{attribute}, roll \e{3d6} and add together the highest two rolls for a value between 2 and 12.

\subsectionx{Fortune}
\e{Fortune} is a mixture of luck, skill and plot armour. To determine starting \e{fortune}, roll a \e{d6}. An \e{agent} starts every session with a number of \e{fortune points} equal to their \e{fortune}.

\subsectionx{Specialisations}
While \e{agents} are generally competent in all fields, each agent starts with a single \e{specialisation}, and when this \e{specialisation} applies to a \e{test} they have an \e{advantage}. \e{Specialisations} are open-form, so feel free to work with the \e{Spymaster} to invent new ones. Some common \e{specialisations} are:

\begin{multicols}{2}
	\small
	\begin{itemize}[noitemsep]
		\item Air vehicles
		\item Blending in
		\item Cryptography/secret signals
		\item Disguise
		\item Electronics (including computers)
		\item Languages
		\item Reflexes
		\item Road vehicles
		\item Running
		\item Seduction
		\item Tailing/spotting tail
		\item Sneak
		\item Strength
		\item Water vehicles
	\end{itemize}
\end{multicols}

\subsectionx{Quirks}
Quirks represent something that causes your \e{agent} to deviate from a ``standard'' \e{agent}. In the MI6 of the 1960s, this is a straight, white, middle- or upper- class man with no vices. Quirks should also, mechanically, be both advantages and disadvantages contextually. Playing a female agent may cause some members of society to act dismissively towards you, but equally dismiss \emph{that such a pretty lady could possibly be here for the reactor plans}. A less contentious example might be that playing an \e{agent} who is alcoholic might necessitate a \e{test} of \e{will} to avoid gravitating to the casino bar, but equally could be used as \e{advantage} for being able to hold one's drink or identify a rare whiskey. \\

What's acceptable and enjoyable to choose as a \e{quirk} will vary greatly from group to group, so be sure to talk to the people you're playing with. The \e{Spymaster} has the last word on anything contentious, as many ``quirks'' can very easily be. The \e{Spymaster} may also just decide that a \e{quirk} isn't necessary; for example, if they're uninterested in having non-player characters act differently towards the aforementioned female \e{agent} in their game then there's no mechanical \e{quirk}. \\

We've found that \e{quirks} work best when you want to take something that others might view as a disadvantage and turn it to \e{advantage} through clever play.

\subsectionx{Advancement}
A player may improve their \e{agent} by spending \e{agent points} between missions. See Rewards in the Mission section for more information on \e{agent points}. \\

Advancement may occur in in the following ways:

\begin{itemize}[noitemsep]
	\item Pay an \e{attribute}’s current value to improve it by 1
	\item Pay the current \e{fortune} value to improve it by \e{d6}
	\item Pay 5 per \e{specialisation} the \e{agent} currently already has to acquire a new \e{specialisation}
\end{itemize}

\SetBgPosition{south}
\backgroundsetup{scale=1, opacity=1, angle=0, placement=bottom, contents={
	\includegraphics[width=\paperwidth]{img/spies}
}}
\BgThispage

%%%%%
\sectionx{Game rules}

\subsectionx{Tests}
When an \e{agent} undertakes a task which might fail, or is put in a situation where they are trying to avoid some negative consequence, the \e{Spymaster}\ can call for a \e{test} against an \e{attribute}. To climb a drainpipe they would \e{test} their \e{body}, to spot a discreet hand-off they would \e{test} their \e{wits}, and to stay focused during a long stakeout they would \e{test} their \e{will}. \\

To \e{test} an \e{attribute}, roll a \e{d20}. If the result is equal to or lower than the \e{attribute} being tested, the agent has succeeded. If they fail, the \e{Spymaster} will adjudicate the circumstances of a failure.

\subsectionx{Advantage \& disadvantage}
If, when a \e{test} is called for, the \e{agent} has an \e{advantage} (such as clever positioning or a relevant \e{specialisation}) then the \e{test} is rolled with \e{2d20} and the better (lower) of the two results is used. Similarly, if the situation puts the \e{agent} at a \e{disadvantage}, then \e{2d20} are rolled and the worst (highest) of the two rolls is used. Equal numbers of \e{advantages} and \e{disadvantages} cancel each other out, with only net \e{advantage} or \e{disadvantage} causing \e{2d20} to be rolled. \\

If the \e{agent} has net two \e{advantages} or \e{disadvantages}, the \e{Spymaster} is encouraged to consider if the result should simply be an automatic success or failure instead, with no roll needed.

\subsectionx{Fortune points}
An agent begins a session with a number of \e{fortune points} equal to their \e{fortune}, and we recommend tracking this with poker chips. This number may go up or down during play, and may even exceed the \e{agent}'s \e{fortune} score as the \e{agent} gathers more \e{fortune points} through roleplay. \\

On a failed \e{test}, the \e{agent} may spend \e{fortune points} to improve the roll on a one-for-one basis to turn it into a success. For example, an \e{agent} who needed to roll 12 or under who rolled a 14 could spend 2 \e{fortune points} to succeed. This should always be roleplayed in an interesting way to represent amazing and uncanny luck, such as revealing that the bullet passed through the \e{agent}'s jacket or that a fall was (against all odds) into a dumpster full of pillows. Coming up with these is \emph{fun} and the whole table is encouraged to join in. \\

When taking \e{attribute} damage, the \e{agent} may spend \e{fortune points} on a one-for-one basis to mitigate the damage. If the damage is reduced to 0, its effects are avoided and the \e{agent} needn't make a \e{traumatic} damage roll. \\

Whenever an \e{agent} does something surpassingly \emph{cool}, that drives the story along or just adds a great deal to the right feeling of suave and sophisticated spycraft, the \e{Spymaster} should reward them with an extra \e{d6} \e{fortune points} for their pool.

\subsectionx{Attribute damage}
Some activities, especially (but not necessarily) from failed \e{tests}, can damage \e{attributes}. For more information, see the Combat section (for \e{body} damage) and the Equipment and gadgets section (alcohol and poisons) for examples. If \e{body} reaches 0, the \e{agent} is dead. If \e{wits} reaches 0, the \e{agent} is unconscious. If \e{will} reaches 0, the agent has lost control of themself and is unable to perform any but a limited set of actions approved by the \e{Spymaster}, suitable for the effect of the \e{will} damage. \\

Further, if the effect is \e{traumatic} rather than gradual -- such as from a bullet wound damaging \e{body} rather than, say, prolonged torture -- the \e{agent} must \e{test} the \e{attribute} using its new (damaged) value. A failure means that the \e{agent} will be neutralised for the remainder of the scene, and effectively unable to contribute anything of value. \e{Fortune points} may be spent on this roll like any other \e{test}. \\

Until damage is recovered, all \e{tests} are against the damaged value.

\subsectionx{Recovery}
After a nights' rest, \e{agents} recover \e{d6} to each damaged attribute and \e{2d6} to their \e{fortune points} up to a maximum of their \e{fortune}. Between missions or sub-chapters of missions with substantial downtime, don't bother rolling and just restore all of these values to full.

\subsectionx{Languages}
Generally \e{agents} speak English, but have some training in foreign languages. If the \e{agent} needs to understand something said in a foreign language \e{test} \e{will}. If the language is a common one for spies of the era (German, Russian) give \e{advantage}.

%%%%%
\sectionx{Action scenes}

\subsectionx{Initiative}
When action starts, the first thing to do is determine who is able to act immediately. This may be obvious, or may require a \e{test}. This \e{test} should vary greatly depending upon the \e{agents} current situation. Some example cases:

\begin{itemize}[noitemsep]
	\item The \e{agents} are searching an office building when a security guard opens the door. \e{Test} \e{wits}!
	\item The \e{agents} are in a standoff, and somebody needs to draw first to break the tension. Hands twitch and... \e{test} \e{body}!
	\item The \e{agents} have spotted their target making his way up the driveway, laying in ambush. As the car rolls up, they act! No \e{test} required.
\end{itemize}

\e{Agents} who have the \e{initiative} are able to \e{act} immediately in the \e{turn sequence}, otherwise they will miss their first action and must start at the \e{react} step.

\subsectionx{Turn sequence}
\e{Agents} switch between \e{acting} and \e{reacting}, doing one after the other. They may \e{act} to succeed at some task, such as shooting at a few guards, leaping from a bridge into a moving car to escape the scene, or anything else. When they \e{react}, they are avoiding the negative effects of those who would act against them -- usually those trying to do them harm, but not necessarily. \\

When \e{acting}, each \e{agent} can attempt a few seconds of action that will require a \e{test} or perform a few seconds of action without a \e{test}. Sometimes a ``few seconds of action'' will necessitate more than one \e{test}, and that's fine. \\

The \e{reacting} stage is to avoid harm. For example: if being fired upon the player might describe their \e{agent} diving behind a nearby crate (a \e{body} \e{test} with \e{advantage}), or tricking them into firing at a mirror (a \e{test} of \e{wits}). A success means the \e{agent} avoids taking damage. \e{Agents} may find themselves facing several threats at once, in which case they may have to make multiple \e{tests} to avoid multiple negative outcomes.

\subsectionx{Weapons}
Shooting a gun or using a finesse weapon generally requires a \e{wits} \e{test}, whereas a machine gun, brawling or heavier melee weapon requires a \e{test} of \e{body}. \\

\subsectionx{Damage}
After successfully \e{acting} or failing when \e{reacting}, enemies and \e{agents} (respectively) suffer the effects of damage. Enemies are divided into \e{minions}, \e{henchmen} and \e{antagonists} and have special rules below. For \e{agents}, \e{henchmen} and \e{antagonists}, a \e{d6} is rolled to determine damage taken -- usually to \e{body}. This roll is then modified for the source of the damage, to a minimum of 1 point.

\begin{multicols}{2}
	\small
	\begin{itemize}[noitemsep]
		\item Unarmed = -2
		\item Improvised weapons = -1
		\item Melee weapons = 0
		\item Most pistols = 0
		\item Heavy pistols = +1
		\item Rifles = +2
		\item Shotguns, machine gun = +3
		\item Vehicle-mounted = +5
	\end{itemize}
\end{multicols}

Remember that because this damage is \e{traumatic}, \e{agents} have a chance of being incapacitated for the rest of the scene. See Attribute Damage in the Game Rules section.

\subsectionx{Minions}
\e{Minions} represent the rank-and-file enemies the \e{agents} will meet. Thugs, \banshee{} soldiers, KGB agents etc.. Generally, \e{minions} operate in groups and a single action from the \e{agents} will neutralise one of them at a time. For example two agents might face eight \banshee{} soldiers with machine guns, and for the purpose of the \e{action scene} the \e{Spymaster} will have them act in two groups of four \e{minions} to harry both \e{agents}. When each \e{agent} takes their shot at a group and succeeds, the \e{agents} neutralise a \banshee{} soldier each and are now facing two groups of three. When \e{reacting}, the \e{agents} are sprayed with machine gun fire. Because the \e{minions} act in groups, the \e{agents} need only make a single \e{test} for the group firing at them. Success means that no \e{minion} in the group has hit them, and failure means that the \e{agent} has been hit by a single roll of machine gun damage.

\subsectionx{Henchmen and antagonists}
\e{Henchmen} and \e{antagonists} operate individually, always being threats on their own. They have \e{fortune points} and a \e{body} \e{attribute}, although both are only used for resisting damage. Once these enemies have run out of \e{fortune points} and begin taking \e{traumatic} damage to \e{body}, they must \e{test} \e{body} or be neutralised just like \e{agents}. \\

\e{Henchmen} and \e{antagonists} may have special traits which make them unique. A tough \e{henchman} might reduce all incoming damage by one, or a martial artist might give \e{disadvantage} for melee attacks and when \e{reacting} to their strikes.

%%%%%
\sectionx{Missions}

\subsectionx{Introductory scenario}
Agents of Fortune is designed with the OSR philosophy in mind, and the scenario ``Ghostlight'' is designed to provide concrete examples of this advice. It's recommended to run (or at least read) that scenario in addition to reading this chapter.

\subsectionx{Player skill}
Players should be encouraged to think through problems to make a situation unfair in their favour where possible. Tricking guards into an ambush, putting a knockout agent in the cocktails and sabotaging the villain's getaway speedboat ahead of time are all great ideas, great fun and totally in the spirit of the game. Don't be afraid to give the \e{agents} \e{advantage} or even an automatic success if their ideas seem like they would work. Let talking in character overrule or massively affect chances of success in social cat-and-mouse, seduction or the like.

\subsectionx{Timeline}
To plan a scenario, first prepare a timeline of events. Treat the villains as goal-oriented; what do they want and, barring the \e{agents}' interference, what will happen and when? The \e{agents} can then cause chaos, and it's the \e{Spymaster}'s job to have the villains course-correct to try to continue to meet their goals. By not assuming the \e{agents}' actions the \e{Spymaster} allows the players free reign to be very creative, and gives themselves a robust framework for an adventure which will make sense no matter what the \e{agents} do. \\

The Alexandrian's "Don't Prep Plots" article is suggested reading, available online via a web search.

\imgplaceholder

\subsectionx{Dossier}
Mystery is of prime importance to villains in the spy genre. Is Sebastiaan Jacobs an agent of \banshee{}, or just a callous businessman? Which of these officials is leaking Ministry of Defense secrets to the USSR? Is the beautiful DNC translator here to ensnare foreign ambassadors for blackmail, or is there something more to her? The \e{agents} should be given some information about the important characters of a scenario, but leave plenty of room for the players to speculate. If you can provide a mocked-up dossier, complete with manilla envelope, that's highly encouraged. The \e{Spymaster}, however, should be clear about the goals and connections of these characters ahead of time, rather than making it up on-the-fly. This will help the character's actions make sense when plans change, and allow the \e{Spymaster} to have them respond appropriately to \e{agent} actions. \\

It's important to give the players something to imagine when interacting with these characters and differentiate them. We've found that many scenarios have enough characters that keeping clear mental pictures of them all can be difficult. You could draw pictures, even just sketches, for the dossier. We often cheat - and a simple web search for a picture of the actor you would have play the character in a movie works well.

\subsectionx{Rewards}
\e{Agents} should be rewarded after a session of play based upon their performance in the mission at hand. Give the \e{agents} a star rating from 1-5 based upon how well they did, and reward an equal number of \e{agent points}. \\

\begin{small}
\begin{tabular}{r p{10cm}}
	\large{\textborn\textborn\textborn\textborn\textborn} & Exceptional achievement. The agents completed their goal, captured the villain(s), fully thwarted their plots, and fully resolved the situation without any negative effects. Good work, agents! \\
	\large{\textborn\textborn\textborn\textborn} & The main villain(s) got away but the agents completed exceptional information gathering or substantially stymied their goals. The villain(s) are hurting, and next steps are clear. \\
	\large{\textborn\textborn\textborn} & The agents had some good successes, but so did the villain(s). The game of cat-and-mouse continues, and the agents must work hard to keep up. \\
	\large{\textborn\textborn} & The villains(s) largely succeeded, or gathered more information on the agents than the agents gathered for themselves. Maybe they now know that MI6 is after them, or the specific identities of the agents, but either way the agents now have a harder task ahead of them. \\
	\large{\textborn} & The agents gathered almost nothing, and the villain(s) achieved their goals fully. Next time the agents will be facing the results of an unhindered and better prepared foe, and have little to show for it.
\end{tabular}
\end{small}

%%%%%
\sectionx{Glossary}
\begin{itemize}[noitemsep,leftmargin=0.75em]
	\item \textsc{\e{acting}} -- the part of a \e{turn} where the \e{agents} are proactively trying to achieve something.
	\item \textsc{\e{action scene}} -- a series of time-critical events represented by multiple \e{turns}.
	\item \textsc{\e{advantage}} -- rolling two \e{d20}s and taking the best (lower) result.
	\item \textsc{\e{agent points}} -- acquired as a reward after a session of play, and spend to improve \e{agents}.
	\item \textsc{\e{agent}} -- a type of character controlled by one of the players.
	\item \textsc{\e{antagonist}} -- a type of character controlled by the \e{Spymaster}. A major threat.
	\item \textsc{\e{attribute}} -- a numerical rating of a character (usually an \e{agent}'s) abilities.
	\item \textsc{\e{banshee}} -- the default evil organisation opposing the \e{agents}.
	\item \textsc{\e{body}} -- an \e{attribute} covering physical fitness and conditioning.
	\item \textsc{\e{disadvantage}} -- rolling two \e{d20}s and taking the worst (higher) result.
	\item \textsc{\e{fortune points}} -- a pool of resources to allow \e{agents} to succeed when they might fail, and avoid damage.
	\item \textsc{\e{fortune}} -- a character's (usually an \e{agent}'s) luck, skill and plot armour. Determines a session's starting \e{fortune points}.
	\item \textsc{\e{henchman}} -- a type of character controlled by the \e{Spymaster}. More important than a \e{henchman}, but less so than an \e{antagonist}.
	\item \textsc{\e{initiative}} -- something that \e{agents} can have, which allows them to start an \e{action scene} in the \e{acting} phase rather than the \e{reacting} phase.
	\item \textsc{\e{minion}} -- a type of character controlled by the \e{Spymaster}. Not important and generally encountered in groups.
	\item \textsc{\e{quirk}} -- an \e{agent}'s distinctive character traits.
	\item \textsc{\e{reacting}} -- a part of a \e{turn} where the \e{agents} are trying to avoid being hurt or otherwise negatively affected by \e{Spymaster} characters.
	\item \textsc{\e{specialisation}} -- a representation of an \e{agent}'s focused skill. Provides \e{advantage} in some situations.
	\item \textsc{\e{spymaster}} -- the player in charge of the game, who runs the others players (who play \e{agents}) through the scenario.
	\item \textsc{\e{test}} -- an \e{d20} roll against an \e{attribute}, where rolling equal or under is a success.
	\item \textsc{\e{traumatic}} -- damage that a character (usually an \e{agent}) takes which can cause them to be unable to act further in an \e{action scene}.
	\item \textsc{\e{turn}} -- a paired \e{acting} and \e{reacting} phase of an \e{action scene}.
	\item \textsc{\e{will}} -- an \e{attribute} covering strength of mind and general learning.
	\item \textsc{\e{wits}} -- an \e{attribute} covering quick thinking and action.
\end{itemize}

\end{document}
